---
title: "Contact"
date: 2023-03-24T00:35:38+02:00
---
The most efficient way to contact me is by email: federicoleva@tiscali.it is my primary address. For specific activities please use the corresponding email address. You may see me as nemowiki@ on mailing lists and Nemo_bis on IRC.

Feel free to ask my cell number, [Telegram](https://www.telegram.org/), Matrix etc.

PGP: public key [52D6171F06278E61FCA90DD3638B7EE3C220E097](/images/52D6171F06278E61FCA90DD3638B7EE3C220E097.asc).

I live in Helsinki.
