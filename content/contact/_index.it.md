---
title: "Contatti"
date: 2023-03-24T00:35:38+02:00
---
Il contatto piú efficiente è via posta elettronica: federicoleva@tiscali.it è il mio indirizzo primario. Per attività specifiche è meglio usare l'indirizzo corrispondente. Scrivo con nemowiki@ nelle liste di discussione e Nemo_bis in IRC.

A richiesta numero di telefono, [Telegram](https://www.telegram.org/), Matrix ecc.

PGP: chiave pubblica [52D6171F06278E61FCA90DD3638B7EE3C220E097](/images/52D6171F06278E61FCA90DD3638B7EE3C220E097.asc).

Il mio domicilio è a Helsinki.
