---
title: "A portrait of the public domain as a puzzle"
date: 2023-03-23T10:20:01+02:00
lastmod: 2023-03-23T15:00:00+02:00
disable_share: true
categories:
- News
tags:
- copyright
- helsinki
- public-domain
- ravensburger
- wiki
---

## Exhibition in Helsinki

As the copyright geek I am, this week I'm celebrating the public domain with a small **exhibition at [Katugalleria Mutteri](https://kalliola.fi/palvelut/tilanvuokraus/tilat/katugalleria-mutteri/)** in Sturenkatu, Helsinki, Finland, featuring various public domain items. The exhibition goes on **from 2023-03-17 to 2023-03-24** at 8:00 in the morning.

The purpose of the exhibition is to advertise the sale of a lovingly used copy of a [Ravensburger 15250](https://web.archive.org/web/20230224093649/https://cdon.fi/lelut/ravensburger-15250-palapeli-kuviopalapeli-1000-kpl-taide-p82798980) 1000-pieces puzzle, featuring a drawing of the _[Uomo Vitruviano](https://en.wikipedia.org/wiki/Vitruvian_Man)_ by Leonardo da Vinci. The copy will be **sold at an auction** with a starting price of 250 €; for now the [item is for sale on **huuto.net**](https://www.huuto.net/kohteet/1000-pieces-puzzle-ravensburger-15250-uomo-vitruviano/578991886) at an outright purchase price of 4999 € (the auction will be opened later).

In order to apply our love to the public domain, I (with some friends and anyone coming by) will attempt to complete the puzzle in a little **puzzle marathon on 2023-03-23** starting some time around 19:00. Depending on the interest and participants' will, we might try to live stream the marathon, or at least record a time lapse video. Based on our initial work with sorting the pieces, we're very unlikely to finish quickly, unless a puzzle wizard turns up: even an expert like the author of the "Karen Puzzles" channel, for all her [experience with solid colour puzzles](https://yewtu.be/watch?v=vfDJhVVeMjU), would probably take at least 4 hours to complete it.

During this effort, we'll be supported by the comforting sound of our playlist of public domain classics such as the court hearings for [Golan v. Holder](https://en.wikipedia.org/wiki/Golan_v._Holder), [Eldred v. Ashcroft](https://en.wikipedia.org/wiki/Eldred_v._Ashcroft) and [Feist Publications, Inc., v. Rural Telephone Service Co.](https://en.wikipedia.org/wiki/Feist_Publications,_Inc.,_v._Rural_Telephone_Service_Co.), which are the gospel of every practicing [Wikimedia Commons PD-Art](https://commons.wikimedia.org/wiki/Commons:When_to_use_the_PD-Art_tag) believer. We may also throw in some [publicdomainradio.org](http://publicdomainradio.org) music or some chapters of David Lange's [*No Law*](https://archive.org/details/nolawintellectua0000lang) if need be. Copies of the [Public Domain Manifesto](https://publicdomainmanifesto.org/) surround us, as a symbolic shield from the hostile world of copyright landlords and as a way for people walking by to join in our blessed activity without getting a headache (or frankly getting a bit frozen, because it snowed this week and there's no heating in the gallery!).

### Images from the exhibition

![Katugalleria Mutteri in Kalliola, Sturenkatu, Helsinki](2023-03-17_Katugalleria_Mutteri_05.JPG)

![The exhibition "A portrait of public domain as a puzzle", as set up on 2023-03-18](2023-03-18_Katugalleria_Mutteri_04.JPG)
    
## Why this puzzle matters

As unlikely as it may sound, this specific puzzle is at the centre of [a **legal controversy in Italy**](https://communia-association.org/2023/03/01/the-vitruvian-man-a-puzzling-case-for-the-public-domain/) and it's subject to a court order which, if I understand correctly, prohibits not only the sale of the puzzle but even the usage of the image and the name of the work, across the world.

The puzzle is part of a small collection of Ravensburger puzzles which unusually feature public domain classics. The collection has existed for many years, but it's a small part of Ravensburger's collections. It's probably easier for a publisher like Ravensburger to commission drawings from artists so that they can have all sorts of copyrights and other legal claims on them, but here they decided to do something more interesting: they tried to turn an old work into something that can also be fun for kids. (Whether this succeeded, I don't know; reviews for the puzzle vary.)

In 2019, the European Parliament and the Council of the EU decided that making the public use the public domain works is a commendable effort and should be encouraged, so they passed [article 14 of directive 790/2019](https://en.wikipedia.org/w/index.php?title=Directive_on_Copyright_in_the_Digital_Single_Market&oldid=1142448338#Article_14), which [mandates](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2019.130.01.0092.01.ENG#d1e1354-92-1) that member states facilitate the usage of such unoriginal images of public domain works as the image of the Vitruvian Man («Member States shall provide that, when the term of protection of a work of visual art has expired, any material resulting from an act of reproduction of that work is not subject to copyright or related rights»). 

In particular the entity which got the puzzle banned in Italy is [Gallerie dell'Accademia di Venezia](https://en.wikipedia.org/wiki/Gallerie_dell%27Accademia), which through the [accidents of history](https://en.wikipedia.org/wiki/Vitruvian_Man#Provenance) happens to have held onto the original of the drawing for a couple centuries now (with some help from Napoleonic wars and enterprising clerics). Before that, the drawing spent a few centuries with the successors of Leonardo's "friend", [Francesco Melzi](https://en.wikipedia.org/wiki/Francesco_Melzi). Currently, the Gallerie are overseen by the Ministry of culture of the Republic of Italy, itself a very recent creation compared to Leonardo's ca. 1492 drawing.

Due to recent rearrangements at the Ministry, the Gallerie became "autonomous". As far as I can understand with a quick look at the limited public information, this mostly means they keep running all the essential operations with the support of the state's budget financed by the taxpayer (as it should be), but on top of that they also have some [curious financials of their own](https://www.gallerieaccademia.it/bilanci) where they do various things with "their own" money, largely extracted from the public through commercial operations. This side hustle of the entity appears to be operating at a substantial loss of several millions euro, which might explain the sudden urge to find financial windfalls through extractive rent-seeking.

We don't know what's the cost of this aggressive legal operation because it's mostly paid for by the taxpayer, which covers unlimited costs for the laywers (the [Avvocatura](https://it.wikipedia.org/wiki/Avvocatura_dello_Stato)) and the courts. However, we know from [an interview to _Repubblica_ quoted by _il Post_](https://www.ilpost.it/2023/02/24/pagare-diritti-puzzle-uomo-vitruviano/) that the Gallerie spent at least the wage of 1 FTE mid-level employee for several years to recoup a total of less than 300 k€, so this entire business of charging the public for reproductions of public domain images is unprofitable even just by looking at its basic operating costs.

Negative externalities on the general interest, [as discussed by professor Daniele Manacorda](https://doi.org/10.13138/2039-2362/2335), are harder to quantify. There doesn't seem to be any attempt to justify the compliance of the Ministry's actions with the [Constitution of the Republic of Italy](https://en.wikisource.org/wiki/Constitution_of_Italy), such as art. 9 (promotion of culture), 21 (freedom of expression) and 33 (freedom of the arts and science), or indeed articles 1 (sovereignty of the people) and 11 (international cooperation).

## Why buy this copy of the puzzle

You can still buy a new copy from a few places around Europe and the world, and you probably should: we're informed by the manufacturer that the puzzle is out of production and out of stock. It's also extremely unlikely to be profitable, given legal costs alone. Therefore it will probably become more and more rare. If the Ministry gets its way, nobody will ever be allowed to have fun with works from centuries past.

But more importantly, this copy is the only one in the world used for this exhibition and performance, which I call _A portrait of the public domain as a puzzle_. Be part of this slightly crazy celebration!

Alternatively, if you prefer something more cheeky, might you be interested in a [plagiarism collection](https://www.techdirt.com/2021/10/25/own-bit-plagiarism-our-plagiarism-collection-nft-auction-close-this-wednesday/) by Techdirt or professor Brian L. Frye?

## Legal disclaimers

A few legal warnings:

1) This sale concerns the physical item. You will not acquire any right on the original image, the design of the puzzle and its box or anything else. The purchase of the item constitutes ownership of 1 physical copy of the work Ravensburger 15250, over which the seller has no rights apart from those given by the purchase of a new copy. The purchase of the item does not constitute ownership of anything other than that. Specifically, it does not constitute ownership of the original work by Leonardo da Vinci, any reproduction produced in its physical presence, any derived reproduction, any images reproduced in this page or the  digital image files illustrating this offering, or any other work of authorship fixed in a tangible medium.
2) I have not paid any royalties on the usage of Leonardo's image, nor will I ask authorization as the court order suggests. However you may decide to do what you wish. No indemnification from any future legal risks is provided.
3) As described above, this puzzle is subject to a court order of the court of Venice ([Ordinanza Tribunale Venezia: caso Ravensburger / Uomo Vitruviano Leonardo](https://doi.org/10.5281/zenodo.7679296)), which follows a similar case about the David, see [*Tutela dei beni culturali e lo strano caso di Studi d'arte Cave Michelangelo*](https://doi.org/10.5281/zenodo.7655286) by Simone Aliprandi and Carlo Piana. No legal advice is provided on the consequences of participating.
4) The sale happens at  the same time as "A portrait of the public domain as a puzzle", a sales announcement and street advertising performance. Arrangements related to this work by the author, Federico Leva, may be made separately. Photos, recordings and other works deriving from the performance will be released by the authors under CC BY-SA 4.0 and published on Wikimedia Commons or archive.org.
5) Only pick-up in person is provided, but different delivery methods can be discussed. In particular, if you're abroad and come pick up the puzzle in person, you may enjoy a weekend of free hospitality at the seller's place in [Vallila, Helsinki](https://www.vallila.org/) (included in the offer: sofa bed; up to 48 hours of puzzle solving; unlimited hours of discussion with a person opinionated about copyright, free knowledge and freedom of expression).
6) This sale happens under Finnish law. As a sale between individuals I believe it's covered by the ["flea market sale" taxation regime](https://www.vero.fi/en/individuals/tax-cards-and-tax-returns/income/earned-income/flea-market-sales/), however this is not tax advice. If you have different needs for fiscal or legal reasons, they can be discussed.

## Miscellaneous

This page can also be found through the shorter URL [leonardo.leva.li](https://leonardo.leva.li/).

You might notice that I finally gave up and added a blog section to this website! Originally I didn't want to commit to the daunting task of keeping a blog online for decades, but I've had this website for many years and I have various places where to host a static website with minimal effort, so I figured it wouldn't harm to expand its usage a bit.
