---
title: "Richiesta di rimozione di Google Analytics ex art. 17 GDPR"
date: 2022-07-08T23:17:00+03:00
lastmod: 2023-03-14T16:00:00+03:00
aliases:
     - "/111742"
---

Questa pagina fornisce informazioni aggiuntive per chi ha ricevuto un mio messaggio a oggetto "Uso illegittimo di Google Analytics: richiesta di rimozione ex art. 17 GDPR".

## Modalità di risposta

Poiché molti titolari del trattamento hanno preferito rispondere per posta elettronica, per evitare ridondanze non è al momento disponibile la modalità di risposta tramite modulo. Mi scuso per il disagio.

Resta possibile rispondere per posta elettronica direttamente all'indirizzo domande@leva.li come precedentemente indicato.

## Approfondimenti

Tutte le informazioni necessarie sono disponibili nel [provvedimento formale del Garante](https://www.garanteprivacy.it/home/docweb/-/docweb-display/docweb/9782874). Di seguito si forniscono alcune risorse aggiuntive.

* [Google: Garante privacy stop all'uso degli Analytics. Dati trasferiti negli Usa senza adeguate garanzie](https://www.garanteprivacy.it/home/docweb/-/docweb-display/docweb/9782874); [Google Analytics, Scorza: "Ecco cosa devono sapere le aziende"](https://www.garanteprivacy.it/web/guest/home/docweb/-/docweb-display/docweb/9789874)
* [Ciao Internet con Matteo Flora: Google Analytics vietato - analizziamo il problema con Guido Scorza del Garante](https://tube.tchncs.de/w/gewJjfEh3HVv8neoWWKKtj)
* Alternative a Google Analytics
  * [Suggerimenti CNIL](https://www.cnil.fr/fr/cookies-solutions-pour-les-outils-de-mesure-daudience) ([in inglese](https://www.cnil.fr/en/google-analytics-and-data-transfers-how-make-your-analytics-tool-compliant-gdpr)); [domande frequenti](https://monitora-pa.it/2022/06/14/faq-cnil.html) e [informazioni sugli identificativi univoci](https://www.cnil.fr/fr/cookies-et-autres-traceurs/regles/alternatives-aux-cookies-tiers).
  * [Software libero](https://fsfe.org/freesoftware/): [Matomo](https://matomo.org/), [Plausible Analytics](https://plausible.io/), [GoatCounter](https://nlnet.nl/project/GoatCounter/)
* Strumento per verificare che il proprio sito sia pulito: [webbkoll](https://webbkoll.dataskydd.net). Inserire il dominio e verificare nelle sezioni "Cookies" e "Third-party requests" che siano assenti GA, "Analytics (Google)" ecc.
* Documentazione Google: [Eliminare un account Analytics](https://support.google.com/analytics/answer/1009696)
* [Introduzione alla differential privacy](https://desfontain.es/privacy/friendly-intro-to-differential-privacy.html), di Damien Desfontaines; [Is differential privacy the right fit for your problem?](https://desfontain.es/privacy/litmus-test-differential-privacy.html)

## Ulteriori approfondimenti legali

* [Regolamento (UE) 2016/679](https://eur-lex.europa.eu/legal-content/IT/TXT/HTML/?uri=CELEX:02016R0679-20160504)
* Approfondimenti legali dalle autorità
  * EDPB: [RGPD: linee guida, raccomandazioni e migliori prassi](https://edpb.europa.eu/our-work-tools/general-guidance/guidelines-recommendations-best-practices_it); [linee guida sull'esercizio dei diritti](https://edpb.europa.eu/system/files/2022-01/edpb_guidelines_012022_right-of-access_0.pdf); [linee guida e relazione di gennaio 2023](https://edpb.europa.eu/news/news/2023/edpb-determines-privacy-recommendations-use-cloud-services-public-sector-adopts_it); [linee guida di febbraio 2023](https://edpb.europa.eu/news/news/2023/edpb-publishes-three-guidelines-following-public-consultation_en)
  * [Google Analytics nei provvedimenti delle autorità](https://gdprhub.eu/index.php?search=%22google+analytics%22&fulltext=1) indicizzati da GDPRhub.
  * [Un caso di "titolarità congiunta"](https://gdprhub.eu/index.php?oldid=23595)
  * Alcune cause USA: [Calhoun v. Google LLC](https://www.courtlistener.com/docket/17386347/calhoun-v-google-llc/) ([vedi estratti](https://mastodon.social/@wchr/108925509988311260)); [K. v. Google LLC](https://www.courtlistener.com/docket/60687806/k-v-google-llc/); [United States v. Google LLC](https://www.courtlistener.com/docket/66753787/united-states-v-google-llc/) ([vedi estratti](https://www.techdirt.com/2023/01/27/the-latest-antitrust-case-against-google-is-by-far-the-most-serious/))
* NOYB: [esercizio art. 17](https://noyb.eu/en/exercise-your-rights-article-17-gdpr-have-your-data-deleted); [101 Complaints on EU-US transfers filed](https://noyb.eu/en/101-complaints-eu-us-transfers-filed)
  * [NOYB on data brokers](https://noyb.eu/en/data-brokers-identification-possible-sell-ads-not-exercise-fundamental-rights), 2023-02-28: «Tracking cookies are used to identify, profile and target a user with personalized ads. Therefore, cookie data can also be used to identify and authenticate users exercising their GDPR rights.»
  * [NOYB WeComply FAQ](https://wecomply.noyb.eu/it/app/faq) (in italiano)

## Dicono di noi

* "Soluzioni poche, a parte disinstallare Analytics, installare Matomo o fingersi morti (eh…) […] Matteo Flora ci spiega benissimo perché (con intervista a Federico, che avremo il piacere di conoscere)." (Veronica Scaletta)
* "La mail, con tutto il rispetto per un interessato che chiede tutela, toglie e aggiunge poco rispetto a quanto deciso già precedentemente. […] La richiesta è fondata se, dentro casa del titolare del trattamento, ci sono dei dati del richiedente. E allora bisognerà accogliere quanto viene fatto notare nella mail. Non vedo grande spazio per il grigio: la richiesta va adempiuta. Forse per evitare di incorrere in richieste simili e portarsi avanti con il lavoro, si potrebbero accorciare i tempi di retention di determinati dati." ([Guido Scorza](https://www.giornalettismo.com/mail-di-federico-leva-analisi-guido-scorza-garante-privacy/))
* "Ma se gli obiettivi e i moventi sono indiscutibili, le modalità di presentazione dell’iniziativa e di conduzione dell’azione, invece, sono state argomento di ampia ed affollata discussione non esente da critiche." (Stefano Gazzella)
* "Argomentazioni, occorre dirlo, piuttosto robuste. Per questo motivo, è il caso che chi abbia ricevuto la mail di Federico Leva, dia una risposta entro i termini di legge." (Giuseppe Croari)
* "Come si può notare la mail è formalmente impeccabile e contiene tutte le informazioni necessarie per procedere all'eliminazione dei dati ad esso associati;" "il mio consiglio (non legale) è di rispondere (educatamente) ad una sua legittima richiesta". (Alessandro De Marchi)
* "Nota personale: ho trovato interessante vedere in che modo, decisamente organico Google Analytics raccoglie i nostri dati. Da qui ne nasce una riflessione che mi fa dire che forse, l'azione di Federico, per quanto “Rumorosa” sia stata altamente sensibilizzante."
* "La buona notizia è che rispondere in maniera opportuna a questa email è molto facile. A primo impatto può sembrare un labirinto pieno di insidie e per giunta con i tombini semi aperti. Ma posso assicurare che è molto meglio ricevere un'e-mail di Federico Leva da Helsinki che vi chiede di rimuovere i suoi dati da Google Analytics, che una raccomandata verde da parte del Garante." (Matteo Z.)
* "Ci dissociamo da tutte quelle assurdità che Federico si è trovato a ricevere, anche se doveva aspettarselo visto quanto ha sollevato non limitandosi a chiedere di fare valere suoi diritti nel modo giusto."
* "Come diversi professionisti stanno argomentando efficacemente sul web, infatti, il mittente di quelle email, Federico Leva, sta semplicemente esercitando un suo diritto." (Infoesse)
* "L'idea del form è quindi molto buona ed agevola i Titolari nei loro adempimenti, ma probabilmente sarebbe stato meglio se questi non fossero presenti su un sito." (Giancarlo Butti)
* "La richiesta ha comunque il pregio di incrementare i livelli di attenzione di molti titolari del trattamento su una questione di tutto rilievo."
* "Per quanto riguarda la mail inviata in modo automatico da Federico Leva, come da lui ammesso in una intervista pubblica, era puramente a scopo divulgativo." (Ernesto Falcone)
* "Attenzione! Non è così e la richiesta non va sottovalutata per il solo fatto che, se ignorata, apre la strada ad un reclamo al Garante, che – ha già fatto sapere – presterà molta attenzione alle segnalazioni degli interessati per arginare l'illecito trasferimento di dati, aprendo indagini ed istruttorie." (Spazio88)

## Alcune risposte ricevute

* "Grazie per la campagna di sensibilizzazione, ci dispiace che tu abbia visitato il nostro sito prima che rimuovessimo Google Analytics. Ad ogni modo i tuoi dati sono stati rimossi, insieme a quelli di tutti gli altri utenti (abbiamo deciso di rimuovere completamente ogni dato di ogni utente)."
* "La ringrazio per la trattazione ben argomentata ed articolata e la segnalazione sulle criticità di Google Analytics. Ho provveduto a richiedere a Google l'eliminazione di tutti i dati registrati dal 20 giugno ad oggi. Ho altresì attivato il blocco degli indirizzi ip da lei indicati."
* "Salve, come da sua richiesta abbiamo provveduto alla cancellazione dei suoi dati da Google Analytics e rimosso tale strumento dal sito web."
* "Evidenzio che non ho avuto modo di distinguere i suoi dati tra tutti gli altri […], pertanto la informo che ho provveduto a far cancellare tutti i dati di qualunque tipo presenti in Google Analytics, e a far rimuovere tutto quanto dai server Google in modo che non rimanesse niente di niente…"
* "In riferimento alla sua segnalazione riguardante l'utilizzo di Google Analytics sul sito [omissis] si dichiara che tale componente è stato disinstallato dal sito in oggetto e che abbiamo, al meglio delle nostre possibilità tecniche, provveduto alla cancellazione dei dati."
* "La ringrazio per la notifica. Ne ho approfittato per eliminare tutti i dati raccolti e sospendere l'attività di raccolta e analisi di Google Analytics."
* "La ringrazio per la sua mail. Abbiamo provveduto ad aggiornare il sito escludendo google analytics ed eliminando il backup."
* "La ringrazio per la segnalazione, e ci tengo ad informarla, non solo di aver provveduto alla cancellazione dei dati che gli interessano, ma anche di tutti gli utenti e gli accessi registrati sul sito web in questione."
* "[Il sito] non è più collegato a Google Analytics. […] È già stata inviata a Google la richiesta di eliminazione di tutti i dati acquisiti tramite il servizio Google Analytics e riguarda tutti gli utenti [del sito] (compresa quindi la sua utenza). Nel ringraziarla per la sua segnalazione, mi auguro che possa continuare a utilizzare in futuro [il sito] sapendo che la privacy degli utenti è la nostra priorità."
* "Buongiorno Federico, dopo aver ricevuto la sua mail, ed avremmo dovuto farlo prima, abbiamo rimosso lo script dalla nostra pagine e cosi resterà in modo definitivo. I suoi dati sono stati eliminati e mi impegno personalmente affinché ciò non si verifichi più. La ringrazio per l'iniziativa e l'attenzione posta al problema."
* "Prima di tutto grazie della Sua segnalazione. Con la presente desideriamo comunicarle che abbiamo cancellato i dati relativi alla Sua visita al sito […]. In aggiunta desideriamo comunicarle che, poiché non siamo interessati ad alcun dato (personale o meno) di coloro che visitano il sito, è stata nostra cura eliminare google analitycs dal sito stesso."
* "Abbiamo provveduto alla cancellazione richiesta col dato da Lei inviato. Volevamo anche ringraziarla per utili note che ci ha suggerito e la informiamo che stiamo prendendo in esame l'utilizzo […]."
* "Gentile Federico Leva, abbiamo provveduto a rimuovere Google Analytics (del quale non facevamo peraltro nessun utilizzo, si trattava di uno dei plugin del sito in fase di costruzione ed evidentemente rimosso male) il giorno stesso che Lei ci ha inviato la sua mail. Non siamo molto esperti, siamo una piccola azienda che si occupa di escursioni. Teniamo molto alla privacy nei nostri utenti, e la ringraziamo per averci segnalato il problema. Abbiamo dato mandato al nostro webmaster di cancellare i dati da Lei richiesti, nonché tutti i dati finora raccolti da GA."
* "Comunque ti faccio un monumento. Sono DPO per una web agency. Dopo le tue letterine sono finalmente riuscita a far estirpare Google Analytics..."
* "Abbiamo richiesto a Google la rimozione di tutti i dati di tutti i nostri utenti, quindi non sono più presenti dati sulla piattaforma e i dati dei cittadini europei sono pertanto al sicuro."
* "Abbiamo proceduto alla richiesta di cancellazione dei suoi dati anche da Google Analytics, come da sua richiesta. […] Abbiamo anche eliminato Google Analytics dal sito come strumento di raccolta dati."
* "Tramite i dati da Lei forniti, non troviamo traccia della Sua visita. Abbiamo comunque richiesto la rimozione dei dati del sito per il giorno [omissis]. La rimozione sarà effettuata entro 24 ore da parte di Google."
* "Abbiamo rimosso Google Analytics dal nostro sito web. È stato rimosso anche Facebook pixel in quanto non utilizzato. Sono state rimosse anche tutte le Property da Google Analytics."
* "Onde evitare successivi eventuali disguidi, ho provveduto ad eliminare interamente la proprietà di qualsiasi versione di Google Analytics collegata al mio sito."
* "La ringrazio per aver ben trattato e argomentato le criticità di Google Analytics. Ho provveduto a richiedere a Google l'eliminazione di tutti i dati registrati di qualsiasi utente siano. Le segnalo inoltre che ho disattivato il servizio Analytics nel nostro sito."
* "La ringrazio per l'attenzione e le informazioni che ci ha fornito riguardo la transizione verso sistemi più sicuri."
* "Il sito non è più collegato a Google Analytics ed è già stata inviata a Google la richiesta di eliminazione dell'account e quindi di tutti i dati acquisiti tramite Analytics."
* "Sono ad informarla che a seguito delle pronunce del Garante a cui Lei faceva riferimento abbiamo deciso di rimuovere totalmente Google Analytics dal nostro sito internet e di provvedere contestualmente alla cancellazione della relativa proprietà (e dunque di tutti i dati raccolti)."
* "In qualità di responsabile del trattamento dei dati, con la presente sono a comunicarLe, seguito sua richiesta, che ho provveduto alla cancellazione dei suoi dati personali dai nostri sistemi informativi e dagli eventuali backup e ovunque essi siano stati trasmessi a causa dell'uso di Google Analytics, precisando inoltre che ho anche provveduto alla completa rimozione dallo stesso di Google Analytics. Infine, con la presente ottempero a quanto specificato al punto 6) della sua richiesta."
* "Come da sua richiesta abbiamo provveduto alla cancellazione dei dati da Google Analytics. La ringraziamo per la mail dettagliata e d'approfondimento, capiamo benissimo quanto da lei esposto e stiamo provvedendo alle dovute valutazioni ed adeguamento in merito.
* "A seguito della Sua risposta, La informiamo di aver cancellato i Suoi dati personali. In ogni caso, conserveremo copia della corrispondenza intercorsa con Lei per documentare il riscontro alla Sua richiesta per il periodo a
tal fine necessario. In ogni caso, Le ricordiamo la possibilità di proporre reclamo a un'autorità di controllo e di proporre ricorso giurisdizionale."
* "Cogliamo anche l'occasione per ringraziarti per gli spunti di riflessione nati da questo evento, stiamo cercando di capire come apportare migliorie al nostro sito."
* "Essendo particolarmente sensibili alla tematica da lei sollevata, ci tengo a sottolineare che abbiamo rimosso lo strumento Google Analytics dal nostro sito web e stiamo valutando l'implementazione di alternative tecniche conformi alle normative UE e alle disposizioni dell'Autorità Garante per la protezione dei dati personali."
* "La ringrazio per aver messo in luce il problema. Il nostro è un sito vetrina, che non aggiorniamo da anni e gli Analytics erano stati inseriti da chi ci aveva fatto il sito e in realtà è uno strumento che non usiamo. 
Ad ogni modo cercherò di studiare e capire come rimuoverlo o cambiare sistema per il futuro e per tutelare i dati di chi visita il sito."
* "La informo inoltre di aver eliminato analytics dal sito per proteggere tutti i potenziali utenti da un uso illegittimo dei dati."
* "In un'ottica di mantenere le attività legate al sito di cui sopra nel pieno rispetto della Privacy degli utenti, ho inoltre deciso di non avvalermi più di alcun servizio di monitoraggio."
* "Alla riapertura degli uffici […] le comunicheremo prima possibile l'esito della richiesta da Lei avanzata. Approfitto della presente comunicazione per farle i miei personali auguri nella sua quotidiana battaglia contro tutti i “poteri forti” 😉".
* "Abbiamo anche provveduto, avendo compreso alcune problematiche relative all'uso di questo strumento, a togliere il codice di Google Analytics dal nostro sito".
* "Le siamo grati di averci fornito ulteriori spunti di riflessione e riferimenti molto utili e come indicato nella mail precedente stiamo valutando e lavorando ad alternative e/o soluzioni adeguate."
* "Le scrivo questa email per comunicarle che ho provveduto a cancellare i suoi dati dal mio pannello di Google Analytics e al tempo stesso, a rimuovere in maniera definitiva GA Google Analytics dal mio sito web."
* "Come richiesto abbiamo provveduto ad eliminare il suo dato dalla piattaforma Google Analytics che, nel frattempo, abbiamo anche provveduto a mettere in standby in attesa di capire l'evolversi della problematica emersa e stiamo anche valutando alternative server-side o cmq 100% europee.  Come Lei, anche noi, abbiamo a cuore gli aspetti inerenti la privacy e stiamo cercando di migliorare il nostro livello di consapevolezza. Come azienda, usiamo le statistiche in modo molto easy, per una rapida panoramica sul numero di affluenze nelle varie pagine e il tasso di rimbalzo. Non usiamo dati per fare azioni di marketing né tantomeno profilazione."
* "Abbiamo ritentuto molto utili gli ulteriori approfondimenti e collegamenti che ci ha inviato. In attesa di terminare le valutazioni su possibili alternative o soluzioni abbiamo rimosso Google Analytics dal sito web."
* "Dato che il sito è principalmente la vetrina per la nostra passione, l'eliminazione ci è sembrata la scelta migliore e pertanto non passeremo nemmeno a GA4 al momento."
* "I suoi dati personali sono stati eliminati da Google Analytics, abbiamo rimosso tale strumento dal sito web sostituendolo con Matomo. Grazie per la campagna di sensibilizzazione."
* "Abbiamo deciso e provveduto di dare il comando per la cancellazione di tutti i dati di tutti gli altri visitatori di ogni tempo. abbiamo inoltre cancellato/eliminato tutto quanto riguarda Google Analytics sia rimuovendo lo strumento dal sito, sia eliminando cancellando tutto di Google Analytics, per non farne più uso, che fra le altre cose non ci è mai stato utile."
* "Ringraziandola per l'ampia azione di sensibilizzazione esercitata, siamo a confermarle sia la cancellazione dei suoi dati, sia la rimozione di Google Analytics dal nostro sito web."
* "Ho ascoltato la sua intervista e la ringrazio per aver portato alla luce il problema di Google Analytics che forse sarebbe passato sottotraccia."
*  "Grazie per la campagna di sensibilizzazione. Ti informiamo che sul nostro sito era presente il plugin di google analytics (era stato installato da chi aveva costruito sito) ma non era stata finalizzata la configurazione dell'account google analytics, e quindi non è stata registrata nessuna attività di nessun utente. Grazie alla tua segnalazione, abbiamo eliminato il plugin in quanto non vogliamo incorrere in nessuna problematica / sanzione, in quanto quel tipo di dati non è di nostro interesse."

## Testo della richiesta originaria

Oggetto: Uso illegittimo di Google Analytics: richiesta di rimozione ex art. 17 GDPR

Spettabile titolare del trattamento dei dati personali, spettabile responsabile della protezione dei dati,

Vi scrivo in quanto utente del sito $SITO per richiedere la rimozione dei miei dati personali, in forza dell'art. 17 ("Diritto alla cancellazione") del regolamento UE 2016/679. Vogliate cortesemente rispondere entro 31 giorni dalla ricezione della presente per confermare l'ottemperanza, come precisato di seguito.

Il vostro sito incorpora Google Analytics, che provvede a trasferire i dati personali di tutti i vostri visitatori a Google negli USA. Con provvedimento del 9 giugno 2022 (9782890), ciò è stato dichiarato illegittimo dall'Autorità Garante per la protezione dei dati personali, come annunciato nel comunicato stampa "Google: Garante privacy stop all'uso degli Analytics. Dati trasferiti negli Usa senza adeguate garanzie". Il Garante «invita tutti i titolari del trattamento a verificare la conformità delle modalità di utilizzo di cookie e altri strumenti di tracciamento utilizzati sui propri siti web, con particolare attenzione a Google Analytics e ad altri servizi analoghi, con la normativa in materia di protezione dei dati personali», e fissa un termine di 90 giorni passati i quali procederà a ulteriori verifiche.
https://www.garanteprivacy.it/home/docweb/-/docweb-display/docweb/9782874

Guido Scorza, componente del Garante, ha ulteriormente illustrato il provvedimento nell'intervista con Matteo Flora "Google Analytics vietato - analizziamo il problema". L'uso di Google Analytics è illegittimo anche in quanto ogni finalità legittima può essere soddisfatta da software libero ospitato in UE e atto a un corretto trattamento dei dati personali, come Matomo, Plausible Analytics o altri raccomandati dall'Autorità francese CNIL, mentre nessuna versione o configurazione di Google Analytics può garantire di non trattare i dati personali in modo illecito.

Alla luce di quanto sopra:
1. preciso che i dati personali oggetto della presente richiesta sono quelli derivanti dalla mia visita del vostro sito nei giorni scorsi, identificabili dal mio indirizzo IP [omissis] e user-agent ("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3803.0 Safari/537.36"), e ogni dato connesso o derivante dagli stessi;
2. richiedo la cancellazione di tali dati personali dai sistemi informativi del vostro responsabile del trattamento e dagli eventuali backup e ovunque essi siano stati trasmessi a causa del vostro uso di Google Analytics, in quanto: a) tale trattamento è illecito e b) tali dati personali non sono necessari a eventuali finalità legittime, come sopra descritto; c) nella misura in cui il trattamento potesse eventualmente essere lecito in forza di un mio consenso, nego di aver prestato il mio informato e valido consenso, che in ogni caso revoco espressamente con la presente; d) qualora i dati fossero asseritamente trattati sulla base di un legittimo interesse, la presente assume valore di opposizione al trattamento oltre che di richiesta di cancellazione;
3. in particolare richiedo la rimozione di qualsiasi registro o copia dei dati personali di cui sopra da parte di Google e ogni altro responsabile di tale trattamento o altro soggetto che li abbia ricevuti, compresi tutti i dati inviati dal mio browser al momento della visita, nonché qualsiasi versione pseudonimizzata dei medesimi e qualsiasi dato aggregato riconducibile ai medesimi o ad altri miei dati personali, come la classificazione in coorti o qualsiasi tipo di identificativo univoco;
4. richiedo altresì, in forza dell'art. 18(1)(d) del regolamento 2016/679, di interrompere immediatamente ogni trattamento di tali dati personali connessi al mio uso passato e futuro del vostro sito, ad esempio provvedendo alla completa rimozione dallo stesso di Google Analytics (in qualsiasi versione e configurazione) e interrompendo ogni uso dei dati prodotti da Google in relazione agli utenti del vostro sito;
5. ove lo riteneste necessario, mi dichiaro disponibile a fornire ulteriori dati utili a identificarmi come la persona a cui fanno riferimento i dati personali di cui sopra, come l'indirizzo IP esatto e la data e ora della visita più recente, nonché i cookie e altri identificativi esibiti da Google in corrispondenza della stessa;
6. richiedo di rispondere a quanto sopra primariamente tramite il modulo collegato sotto, fornito tramite software libero LimeSurvey ospitato in UE (e rispettoso della privacy), entro 31 giorni dalla ricezione della presente; il mio indirizzo di posta elettronica per questa materia è domande@leva.li.

In fede,

Federico Leva

Helsinki, 29 giugno 2022

## Dati supplementari

Anche per chi non ha ricevuto una richiesta, alla pagina [Web requests analysis of Italy websites which use Google Analytics](https://doi.org/10.5281/zenodo.6974877) in Zenodo sono disponibili alcuni dati, compresa una copia del modulo inizialmente inviato ai destinatari.
